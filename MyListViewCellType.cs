﻿using GrapeCity.Forguncy.CellTypes;
using GrapeCity.Forguncy.Plugin;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace MyListViewCellType
{
    [Designer("MyListViewCellType.MyListViewCellTypeDesigner,MyListViewCellType")]
    public class MyListViewCellType : CellType
    {
        public string TableName
        {
            get; set;
        }
        public string TextColumn
        {
            get; set;
        }
    }


    public class MyListViewCellTypeDesigner : CellTypeDesigner<MyListViewCellType>
    {
        public override EditorSetting GetEditorSetting(PropertyDescriptor property, IBuilderContext builderContext)
        {
            if (property.Name == "TableName")
            {
                return new TableComboTreeSelectorEditorSetting();
            }
            if (property.Name == "TextColumn")
            {
                var columns = builderContext.EnumAllTableInfos().FirstOrDefault(t => t.TableName == this.CellType.TableName)?.Columns?.Select(c => c.ColumnName);
                return new ComboEditorSetting(columns);
            }
            if (property.Name == "ValueColumn")
            {
                var columns = builderContext.EnumAllTableInfos().FirstOrDefault(t => t.TableName == this.CellType.TableName)?.Columns?.Select(c => c.ColumnName);
                return new ComboEditorSetting(columns);
            }
            return base.GetEditorSetting(property, builderContext);
        }

        public override FrameworkElement GetDrawingControl(ICellInfo cellInfo, IDrawingHelper drawingHelper)
        {
            ListBox listBox = new ListBox();
            //get table data for preview.
            var tableData = drawingHelper.GetTableDataForPreview(this.CellType.TableName, new List<string>() { this.CellType.TextColumn }, null, true);
            if (tableData != null)
            {
                foreach (var row in tableData)
                {
                    var value = row[this.CellType.TextColumn];
                    if (value != null)
                    {
                        ListBoxItem item = new ListBoxItem() { Content = value };
                        listBox.Items.Add(item);
                    }
                }
            }
            Grid container = new Grid();
            container.Children.Add(listBox);
            return container;
        }
    }
}
