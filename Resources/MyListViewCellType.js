﻿var MyListViewCellType = (function (_super) {
    __extends(MyListViewCellType, _super);
    function MyListViewCellType() {
        return _super !== null && _super.apply(this, arguments) || this;
    }

    MyListViewCellType.prototype.createContent = function () {
        var self = this;

        var element = this.CellElement;
        var cellTypeMetaData = element.CellType;
        var container = $("<div id='" + this.ID + "'></div>");

        var innerContainer = $(`
<div style='width:100%;height:100%'>
    Custom Cell
</div>`);

        container.append(innerContainer);

        return container;
    };

    MyListViewCellType.prototype.getValueFromElement = function () {
        return null;
    };

    MyListViewCellType.prototype.setValueToElement = function (element, value) {

    };

    MyListViewCellType.prototype.disable = function () {
        _super.prototype.disable.call(this);
    };

    MyListViewCellType.prototype.enable = function () {
        _super.prototype.enable.call(this);
    };

    return MyListViewCellType;
}(Forguncy.CellTypeBase));

// Key format is "Namespace.ClassName, AssemblyName"
Forguncy.Plugin.CellTypeHelper.registerCellType("MyListViewCellType.MyListViewCellType, MyListViewCellType", MyListViewCellType);